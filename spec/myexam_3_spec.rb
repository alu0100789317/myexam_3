require 'lib/myexam_3.rb'
require 'lib/myexam_3.1.rb'

describe SeleccionSimple do
	before :each do
		@p1 = SeleccionSimple.new("Salida del siguiente codigo: Class Xyz \ndef pots \n@nice \nend \nend", ["#<Xyz:0xa000208>","nil","0", "ninguna de las anteriores"])		
		@p2 = VerdaderoFalso.new("Definicion de un has en Ruby: hash_raro = \n[1, 2, 3] => Object.new(), \nHash.new => :toto", nil)	
		@p3 = SeleccionSimple.new("Salida del siguiente codigo: Class Array \ndef say_hi \nHEY! \nend \nend", ["1","bob","HEY!","ninguna de las anteriores"])
		@p4 = SeleccionSimple.new("Tipo de objeto en: \nclass Objeto \nend", ["Una instancia de la clase","una constante","un objeto","Ninguna de la anteriores"])
		@p5 = VerdaderoFalso.new("Es apropiado que una clase Tablero herede de una clase Juego",nil)
	end

	describe "Simple Selection" do

		it "Debe existir una pregunta" do
			expect(@p1.class).to eq(SeleccionSimple)
			expect(@p2.class).to eq(VerdaderoFalso)	
		end
		it "Se debe invocar a un metodo para obtener la pregunta" do
			expect(@p1.pregunta).to eq("Salida del siguiente codigo: Class Xyz \ndef pots \n@nice \nend \nend")
			expect(@p2.pregunta).to eq("Definicion de un has en Ruby: hash_raro = \n[1, 2, 3] => Object.new(), \nHash.new => :toto")
		end
		it "Deben existir opciones de respuesta" do
			expect(@p1.class).to eq(SeleccionSimple)
			expect(@p2.class).to eq(VerdaderoFalso)
		end
		it "Se debe invocar a un metodo para obtener las opciones de respuesta" do
			expect(@p1.respuestas).to eq(["#<Xyz:0xa000208>", "nil", "0", "ninguna de las anteriores"])
			expect(@p2.respuestas).to eq(["Verdadero","Falso"])
		end
		it "Se deben mostrar por la consola formateada la pregunta y las opciones de respuesta" do
			expect(@p1.to_s).to eq("Salida del siguiente codigo: Class Xyz \ndef pots \n@nice \nend \nend\n a:#<Xyz:0xa000208>\n b:nil\n c:0\n d:ninguna de las anteriores")
			expect(@p2.to_s).to eq("Definicion de un has en Ruby: hash_raro = \n[1, 2, 3] => Object.new(), \nHash.new => :toto\n a: Verdadero \n b: Falso")
			expect(@p3.to_s).to eq("Salida del siguiente codigo: Class Array \ndef say_hi \nHEY! \nend \nend\n a:1\n b:bob\n c:HEY!\n d:ninguna de las anteriores")
			expect(@p4.to_s).to eq("Tipo de objeto en: \nclass Objeto \nend\n a:Una instancia de la clase\n b:una constante\n c:un objeto\n d:Ninguna de la anteriores")
			expect(@p5.to_s).to eq("Es apropiado que una clase Tablero herede de una clase Juego\n a: Verdadero \n b: Falso")
		end
	end
	before :each do
		@node1 = Node.new(@p1,nil)
		@node2 = Node.new(@p2,nil)
		@node3 = Node.new(@p3,nil)
		@node4 = Node.new(@p4,nil)
		@node5 = Node.new(@p5,nil)
		@lista = Lista.new(@node1)
		@lista1 = Lista.new(nil)
	end
	describe "Node" do
		it "Debe existir un Nodo de la lista con sus datos" do
			expect(@lista.inicio).to_not eq(nil)
		end
	end
	describe "List" do
		it "Se extrae el primer elemento de la lista" do
			@lista.push_final(@node2)
			expect(@lista.pop_inicio()).to eq(@node1.value)
		end
		it "Se puede insertar un elemento" do
			@lista.push_final(@node3)
			expect(@lista.getFinalValue).to eq(@node3.value)
		end
		it "Se pueden instertar varios elementos" do
			@lista1.push_final(@node1)
			@lista1.push_final(@node2)
			@lista1.push_final(@node3)
			@lista1.push_final(@node4)
			@lista1.push_final(@node5)
			expect(@lista1.getInicioValue).to eq(@node1.value)
			expect(@lista1.getFinalValue).to eq(@node5.value)
			expect(@lista1.pop_final()).to eq(@node5.value)
			expect(@lista1.pop_inicio()).to eq(@node1.value)
		end
		it "Debe existir una lista con su cabeza" do
			expect(@lista.inicio.value).to eq(@lista.getInicioValue)
		end
	end
	
end
