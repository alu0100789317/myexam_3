class SeleccionSimple

	attr_accessor :pregunta, :respuestas

	def initialize pregunta, respuestas
 		@pregunta, @respuestas = pregunta, respuestas
	end
	
	def to_s
		"#{@pregunta}\n a:#{@respuestas[0]}\n b:#{@respuestas[1]}\n c:#{@respuestas[2]}\n d:#{@respuestas[3]}"
	end 
end

class VerdaderoFalso < SeleccionSimple
	
	def initialize pregunta, respuestas
		super(pregunta,["Verdadero","Falso"])
	end
	
	def to_s
		"#{@pregunta}\n a: Verdadero \n b: Falso"
	end
end
